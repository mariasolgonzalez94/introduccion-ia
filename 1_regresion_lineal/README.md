## 1. Objetivo:

Realizar un análisis de datos sobre distintos datasets y ver su distribución. Luego, analizar la técnica   de   Regresión   Lineal   y   a   partir   de   esta,   obtener   modelos   que   permitan   predecir   elcomportamiento de variables

### 1.2 ¿Qué es la regresión lineal?

La regresión lineal es un algoritmo de aprendizaje supervisado que se utiliza en Machine Learning y en estadística. En su versión más sencilla, lo que haremos es “dibujar una recta” que nos indicará la tendencia de un conjunto de datos continuos (si fueran discretos, utilizaríamos Regresión Logística).
En estadísticas, regresión lineal es una aproximación para modelar la relación entre una variable escalar dependiente “y” y una o mas variables explicativas nombradas con “X”.

### 1.3 Fórmula:

$$y = mX + b$$ 

Donde Y es el resultado, X es la variable, m la pendiente (o coeficiente) de la recta y b la constante o también conocida como el “punto de corte con el eje Y” en la gráfica (cuando X=0)

Bibliografía consultada: https://www.aprendemachinelearning.com/regresion-lineal-en-espanol-con-python/
